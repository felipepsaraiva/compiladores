#!/usr/bin/env python3
import sys
from os import path
from pprint import pprint

if len(sys.argv) < 2:
    print('Quantidade de parâmetros insuficiente!')
    exit()

fonte = sys.argv[1]
if not path.exists(fonte):
    print('Arquivo fonte "{}" não existe!'.format(fonte))
    exit()

import lexico
import simbolos

print('Analisando lexicamente o arquivo "{}"...\n'.format(fonte))
print('Tokens reconhecidos:')
while True:
    r = lexico.proximoToken()
    print(r)

    if (r['token'] == 'EOF' or r['token'] == 'ERRO'):
        break

print('\nTabela de Símbolos:')
pprint(simbolos.tabela)
