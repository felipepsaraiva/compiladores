import tabelaSintatica
import lexico

# A ordem é importante, pois primeiro aparecem nao-terminais que representam menores partes
# do programa e quanto menor for a parte elimidada, mais completa será a análise
sync = ('D', 'LV', 'V', 'ES', 'CMD', 'CABEÇALHO', 'CORPO', 'COND', 'A')
seguinte = {
    'A': ('eof'),
    'V': ('leia', 'escreva', 'id', 'se', 'fim'),
    'LV': ('leia', 'escreva', 'id', 'se', 'fim'),
    'D': ('id', 'varfim'),
    'ES': ('leia', 'escreva', 'id', 'se', 'fim', 'fimse'),
    'CMD': ('leia', 'escreva', 'id', 'se', 'fim', 'fimse'),
    'COND': ('leia', 'escreva', 'id', 'se', 'fim', 'fimse'),
    'CABEÇALHO': ('leia', 'escreva', 'id', 'se', 'fimse'),
    'CORPO': ('leia', 'escreva', 'id', 'se', 'fim', 'fimse')
}

def panico(pilha, entrada):
    """Manipula a pilha e descarta entradas de forma que seja possível continuar a análise sintática"""
    # Encontra um estado que possui uma transição para um dos nao terminais selecionados
    for i in range(len(pilha) - 1, -1, -1):
        s = melhorSync(pilha[i])
        if s: break

    # Desempilha todos os estados até que o estado encontrado seja o topo
    # e empilha o estado resultante da transição
    pilha = pilha[:i+1]
    topo = pilha[-1]
    pilha.append(tabelaSintatica.transicoes[s][topo])

    # Elimina entradas até encontrar uma que esteja no conjunto seguinte de 'A'
    while True:
        entrada = lexico.proximoToken()
        if (entrada['token'] in seguinte[s]) or (entrada['token'] == 'eof'): break

    return pilha, entrada


def melhorSync(estado):
    """Retorna o primeiro não terminal da tupla para o qual o estado tem transição"""
    for s in sync:
        if tabelaSintatica.transicoes[s][estado]:
            return s

    return None
