#!/usr/bin/env python3
import sys
from os import path

if len(sys.argv) < 2:
    print('Quantidade de parâmetros insuficiente!')
    exit()

fonte = sys.argv[1]
if not path.exists(fonte):
    print('Arquivo fonte "{}" não existe!'.format(fonte))
    exit()

from sintatico import sintatico
print('Analisando sintaticamente o arquivo "{}"...\n'.format(fonte))
sintatico()
