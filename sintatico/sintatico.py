from tabelaSintatica import acoes, transicoes, producoes
import lexico
import erro

# Algoritmo padrão de interpretação da tabela sintática shift-reduce
def sintatico():
    erros = []
    pilha = [0]
    a = lexico.proximoToken()

    while (1):
        estado = pilha[-1]

        # Tratamento de erros lexicos
        if a['token'] == 'ERRO':
            msg = '[ERRO Ln {}, Col {}] {}.'.format(a['linha'], a['coluna'], a['descricao'])
            print(msg)
            erros.append(msg)
            pilha, a = erro.panico(pilha, a)
            continue

        acao = acoes[a['token']][estado]

        if acao[0] == 'S':
            pilha.append(acao[1])
            a = lexico.proximoToken()
        elif acao[0] == 'R':
            producao = producoes[acao[1]-1]
            parte = producao.split(' -> ')

            quantidade = len(parte[1].split(' '))
            del pilha[-quantidade:]

            estado = pilha[-1]
            pilha.append(transicoes[parte[0]][estado])
            print(producao)
        elif acao[0] == 'A':
            break
        else:
            lexema = '"{}"'.format(a['lexema'])
            descricao = acao[1].replace('%l', lexema)
            msg = '[ERRO Ln {}, Col {}] {}.'.format(a['linha'], a['coluna'], descricao)

            print(msg)
            erros.append(msg)
            pilha, a = erro.panico(pilha, a)
            if a['token'] == 'eof': break

    if len(erros):
        print('\nLista de erros:')
        print('\n'.join(erros))
    else:
        print('\nSintaxe aceita!')

if __name__ == '__main__':
    sintatico()
