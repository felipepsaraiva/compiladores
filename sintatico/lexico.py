# Replica do analisador sintático implementado anteriormente
import sys
import re
import simbolos


f = open(sys.argv[1]) # Arquivo fonte
linha = 1 # Linha atual do arquivo fonte
coluna = 1 # Coluna atual do arquivo fonte


# Regex que descreve cada uma das possíveis entradas
entradas = (
    re.compile(r'[a-zA-DF-Z]'),  # 0
    re.compile(r'\d'),  # 1
    re.compile(r'E'),   # 2
    re.compile(r'_'),   # 3
    re.compile(r'\.'),  # 4
    re.compile(r'"'),   # 5
    re.compile(r'\('),  # 6
    re.compile(r'\)'),  # 7
    re.compile(r'{'),   # 8
    re.compile(r'}'),   # 9
    re.compile(r';'),   # 10
    re.compile(r'\+'),  # 11
    re.compile(r'-'),   # 12
    re.compile(r'\*'),  # 13
    re.compile(r'\/'),  # 14
    re.compile(r'<'),   # 15
    re.compile(r'>'),   # 16
    re.compile(r'='),   # 17
    # Outros 18
    # EOF 19
)

# Tradução do index do estado para o token
estadosFinais = (
    None,           # 0
    'num',          # 1
    None,           # 2
    None,           # 3
    None,           # 4
    None,           # 5
    'literal',      # 6
    None,           # 7
    'comentario',   # 8
    'id',           # 9
    'opm',          # 10
    'ab_p',         # 11
    'fc_p',         # 12
    'pt_v',         # 13
    'opr',          # 14
    'opr',          # 15
    'opr',          # 16
    'rcb',          # 17
    'eof'           # 18
)

erros = (
    'Caractere inválido',
    None,
    'Não há número(s) após o ponto decimal',
    'Não há número(s) após o exponencial',
    'Não há número(s) após o exponencial',
    'Não existe " para finalizar o literal',
    None,
    'Não existe } para finalizar o comentario'
    # Todos os estados seguintes são finais
)

# Tabela de transições
# Linha: Estado / Coluna: Entrada
# Entradas: L D E _ . " ( ) { } ; + - * / < > = spc \t \n Outros EOF
tabelaTransicoes = (
    (9, 1, 9, None, None, 5, 11, 12, 7, None, 13, 10, 10, 10, 10, 15, 16, 14, None, 18),
    (None, 1, 3, None, 2, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    (None, 1, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    (None, 1, None, None, None, None, None, None, None, None, None, 4, 4, None, None, None, None, None, None, None),
    (None, 1, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    (5, 5, 5, 5, 5, 6, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, None),
    (None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    (7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 7, 7, 7, 7, 7, 7, 7, 7, 7, None),
    (None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    (9, 9, 9, 9, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    (None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    (None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    (None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    (None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    (None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    (None, None, None, None, None, None, None, None, None, None, None, None, 17, None, None, None, 14, 14, None, None),
    (None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, 14, None, None),
    (None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    (None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None)
)

def proximoToken():
    global f, coluna, linha, estadosFinais, tabelaTransicoes
    estado = 0
    buffer = ''

    while True:
        posAnterior = f.tell() # Posição no arquivo antes de ler o proximo caractere
        char = f.read(1)

        if (estado == 0) and (char in [' ', '\t', '\n']):
            coluna += 1
            if char == '\n':
                linha += 1
                coluna = 1
            continue # Ignora espaços, tabs e novas linhas

        index = getIndexEntrada(char)
        novoEstado = tabelaTransicoes[estado][index]

        if novoEstado:
            buffer += char
            estado = novoEstado
        else:
            if (estado == 0):
                buffer += char
            else:
                f.seek(posAnterior)
            break

    token = estadosFinais[estado]
    if token == 'comentario':
        coluna += len(buffer)
        return proximoToken() # Ignora o comentario

    if not token:
        unidade =  {'token': 'ERRO', 'lexema': buffer, 'descricao': erros[estado]}
    else:
        unidade = {'token': token, 'lexema': buffer, 'tipo': None}

    if token == 'id':
        if buffer in simbolos.tabela:
            unidade = simbolos.tabela[buffer]
        else:
            simbolos.tabela[buffer] = unidade

    completo = dict(list(unidade.items()) + [('linha', linha), ('coluna', coluna)])
    coluna += len(buffer)
    return completo


def getIndexEntrada(char):
    """Traduz o caractere de entrada para o index da coluna da tabela de transições"""
    global entradas

    if not char:
        return 19  # EOF

    for i, regex in enumerate(entradas):
        if regex.match(char):
            return i

    return 18 # Outra entrada
