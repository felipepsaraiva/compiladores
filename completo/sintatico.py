import sys
import os
import lexico
import semantico
import erros

acoes = {
    'inicio': ('S2','E1','E2','E2','E2','E14','E2','E2','E2','E14','E16','E17','E18','E2','E19','E2','E2','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','E2','E2','E2','E2','E2','E23','E2','E2','E20','E20','E20','E20','E2','E2','E20','E25','E26','E26','E2','E2','E2','E27','E28','E2','E2','E29','E30','E29','E20','E2','E27'),
    'varinicio': ('E0','E1','S4','E4','E4','E14','E4','E4','E4','E14','E16','E17','E18','E4','E19','E4','E4','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','E4','E4','E4','E4','E4','E23','E4','E4','E20','E20','E20','E20','E4','E4','E20','E25','E26','E26','E4','E4','E4','E27','E28','E4','E4','E29','E30','E29','E20','E4','E27'),
    'varfim': ('E0','E1','E3','E5','S17','E14','E5','E5','E5','E14','E16','E17','E18','E5','E19','E5','S17','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','E5','E5','E5','E5','E5','E23','E5','E5','E20','E20','E20','E20','E5','E5','E20','E25','E26','E26','E5','E5','E5','E27','E28','R6','E5','E29','E30','E29','E20','E5','E27'),
    'escreva': ('E0','E1','E3','S11','E11','E14','S11','S11','S11','E14','E16','E17','E18','S11','E19','R3','E11','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','R23','S11','S11','S11','R29','E23','R4','R5','E20','E20','E20','E20','R11','R12','E20','E25','E26','E26','R26','R27','R28','E27','E28','E11','R17','E29','E30','E29','E20','R24','E27'),
    'leia': ('E0','E1','E3','S10','E11','E14','S10','S10','S10','E14','E16','E17','E18','S10','E19','R3','E11','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','R23','S10','S10','S10','R29','E23','R4','R5','E20','E20','E20','E20','R11','R12','E20','E25','E26','E26','R26','R27','R28','E27','E28','E11','R17','E29','E30','E29','E20','R24','E27'),
    'se': ('E0','E1','E3','S14','E11','E14','S14','S14','S14','E14','E16','E17','E18','S14','E19','R3','E11','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','R23','S14','S14','S14','R29','E23','R4','R5','E20','E20','E20','E20','R11','R12','E20','E25','E26','E26','R26','R27','R28','E27','E28','E11','R17','E29','E30','E29','E20','R24','E27'),
    'entao': ('E0','E1','E3','E6','E11','E14','E6','E6','E6','E14','E16','E17','E18','E6','E19','E6','E11','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','E6','E6','E6','E6','E6','E23','E6','E6','E20','E20','E20','E20','E6','E6','E20','E25','E26','E26','E6','E6','E6','E27','E28','E11','E6','E29','S57','E29','E20','E6','E27'),
    'senao': ('E0','E1','E3','E6','E11','E14','E6','E6','E6','E14','E16','E17','E18','E1','E19','E6','E11','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','E1','E1','E1','E1','E1','E23','E6','E6','E20','E20','E20','E20','E6','E6','E20','E25','E26','E26','E6','E6','E6','E27','E28','E11','E6','E29','E30','E29','E20','E1','E27'),
    'fimse': ('E0','E1','E3','E6','E11','E14','E6','E6','E6','E14','E16','E17','E18','S32','E19','E6','E11','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','R23','S32','S32','S32','R29','E23','E6','E6','E20','E20','E20','E20','R11','R12','E20','E25','E26','E26','R26','R27','R28','E27','E28','E11','R17','E29','E30','E29','E20','R24','E27'),
    'fim': ('E0','E1','E12','S9','E12','E15','S9','S9','S9','E15','E16','E17','E18','E12','E19','R3','E12','E20','E21','E15','E15','E15','E20','E20','E20','E20','E20','E22','R23','E12','E12','E12','R29','E23','R4','R5','E20','E20','E20','E20','R11','R12','E20','E25','E26','E26','R26','R27','R28','E27','E28','E12','R17','E29','E30','E29','E20','E12','E27'),
    'int': ('E0','E1','E3','E7','E13','E14','E7','E7','E7','E14','E16','E17','E18','E7','E19','E7','E13','E20','S37','E14','E14','E14','E20','E20','E20','E20','E20','E22','E7','E7','E7','E7','E7','E23','E7','E7','E20','E20','E20','E20','E7','E7','E20','E25','E26','E26','E7','E7','E7','E27','E28','E13','E7','E29','E30','E29','E20','E7','E27'),
    'real': ('E0','E1','E3','E7','E13','E14','E7','E7','E7','E14','E16','E17','E18','E7','E19','E7','E13','E20','S38','E14','E14','E14','E20','E20','E20','E20','E20','E22','E7','E7','E7','E7','E7','E23','E7','E7','E20','E20','E20','E20','E7','E7','E20','E25','E26','E26','E7','E7','E7','E27','E28','E13','E7','E29','E30','E29','E20','E7','E27'),
    'lit': ('E0','E1','E3','E7','E13','E14','E7','E7','E7','E14','E16','E17','E18','E7','E19','E7','E13','E20','S39','E14','E14','E14','E20','E20','E20','E20','E20','E22','E7','E7','E7','E7','E7','E23','E7','E7','E20','E20','E20','E20','E7','E7','E20','E25','E26','E26','E7','E7','E7','E27','E28','E13','E7','E29','E30','E29','E20','E7','E27'),
    'num': ('E0','E1','E3','E8','E11','E14','E8','E8','E8','E14','E16','S25','E18','E8','E19','E8','E11','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','S45','E8','E8','E8','E8','E8','S45','E8','E8','E20','E20','E20','E20','E8','E8','E20','E25','E26','E26','E8','E8','E8','E27','E28','E11','E8','S45','E30','S45','E20','E8','E27'),
    'literal': ('E0','E1','E3','E8','E11','E14','E8','E8','E8','E14','E16','S24','E18','E8','E19','E8','E11','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','E8','E8','E8','E8','E8','E23','E8','E8','E20','E20','E20','E20','E8','E8','E20','E25','E26','E26','E8','E8','E8','E27','E28','E11','E8','E29','E30','E29','E20','E8','E27'),
    'id': ('E0','E1','E3','S12','S18','E14','S12','S12','S12','E14','S22','S26','E18','S12','E19','R3','S18','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','S44','R23','S12','S12','S12','R29','S44','R4','R5','E20','E20','E20','E20','R11','R12','E20','E25','E26','E26','R26','R27','R28','E27','E28','R6','R17','S44','E30','S44','E20','R24','E27'),
    'opr': ('E0','E1','E3','E9','E11','E14','E9','E9','E9','E14','E16','E17','E18','E9','E19','E9','E11','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','E9','E9','E9','E9','E9','E24','E9','E9','E20','E20','E20','E20','E9','E9','E20','E25','R20','R21','E9','E9','E9','E27','S55','E11','E9','E29','E30','E29','E20','E9','E27'),
    'rcb': ('E0','E1','E3','E9','E11','E14','E9','E9','E9','E14','E16','E17','S27','E9','E19','E9','E11','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','E9','E9','E9','E9','E9','E23','E9','E9','E20','E20','E20','E20','E9','E9','E20','E25','E26','E26','E9','E9','E9','E27','E28','E11','E9','E29','E30','E29','E20','E9','E27'),
    'opm': ('E0','E1','E3','E9','E11','E14','E9','E9','E9','E14','E16','E17','E18','E9','E19','E9','E11','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','E9','E9','E9','E9','E9','E23','E9','E9','E20','E20','E20','E20','E9','E9','E20','S53','R20','R21','E9','E9','E9','E27','E28','E11','E9','E29','E30','E29','E20','E9','E27'),
    'ab_p': ('E0','E1','E3','E10','E11','E14','E10','E10','E10','E14','E16','E17','E18','E10','S33','E10','E11','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','E10','E10','E10','E10','E10','E23','E10','E10','E20','E20','E20','E20','E10','E10','E20','E25','E26','E26','E10','E10','E10','E27','E28','E11','E10','E29','E30','E29','E20','E10','E27'),
    'fc_p': ('E0','E1','E3','E10','E11','E14','E10','E10','E10','E14','E16','E17','E18','E10','E19','E10','E11','E20','E21','E14','E14','E14','E20','E20','E20','E20','E20','E22','E10','E10','E10','E10','E10','E23','E10','E10','E20','E20','E20','E20','E10','E10','E20','E25','R20','R21','E10','E10','E10','S54','E28','E11','E10','E29','E30','E29','E20','E10','R25'),
    'pt_v': ('E0','E1','E3','E10','E10','E14','E10','E10','E10','E14','E16','E17','E18','E10','E19','E10','E10','S35','E21','E14','E14','E14','S40','S41','R13','R14','R15','E22','E10','E10','E10','E10','E10','E23','E10','E10','S51','R7','R8','R9','E10','E10','S52','R19','R20','R21','E10','E10','E10','E27','E28','E10','E10','E29','E30','E29','R18','E10','E27'),
    'eof': ('E1','A0','E1','E1','E1','R2','E1','E1','E1','R30','E1','E1','E1','E1','E1','E1','E1','E1','E1','R10','R16','R22','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1','E1')
}

transicoes = {
    'P': (1, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    'V': (None, None, 3, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    'LV': (None, None, None, None, 15, None, None, None, None, None, None, None, None, None, None, None, 34, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    'D': (None, None, None, None, 16, None, None, None, None, None, None, None, None, None, None, None, 16, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    'TIPO': (None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, 36, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    'A': (None, None, None, 5, None, None, 19, 20, 21, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    'ES': (None, None, None, 6, None, None, 6, 6, 6, None, None, None, None, 29, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, 29, 29, 29, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    'ARG': (None, None, None, None, None, None, None, None, None, None, None, 23, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    'CMD': (None, None, None, 7, None, None, 7, 7, 7, None, None, None, None, 30, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, 30, 30, 30, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    'LD': (None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, 42, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    'OPRD': (None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, 43, None, None, None, None, None, 50, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, 56, None, 58, None, None, None),
    'COND': (None, None, None, 8, None, None, 8, 8, 8, None, None, None, None, 31, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, 31, 31, 31, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    'CABEÇALHO': (None, None, None, 13, None, None, 13, 13, 13, None, None, None, None, 13, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, 13, 13, 13, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    'EXP_R': (None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, 49, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None),
    'CORPO': (None, None, None, None, None, None, None, None, None, None, None, None, None, 28, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, 46, 47, 48, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None)
}

producoes = (
    "P' -> P",
    "P -> inicio V A",
    "V -> varinicio LV",
    "LV -> D LV",
    "LV -> varfim pt_v",
    "D -> id TIPO pt_v",
    "TIPO -> int",
    "TIPO -> real",
    "TIPO -> lit",
    "A -> ES A",
    "ES -> leia id pt_v",
    "ES -> escreva ARG pt_v",
    "ARG -> literal",
    "ARG -> num",
    "ARG -> id",
    "A -> CMD A",
    "CMD -> id rcb LD pt_v",
    "LD -> OPRD opm OPRD",
    "LD -> OPRD",
    "OPRD -> id",
    "OPRD -> num",
    "A -> COND A",
    "COND -> CABEÇALHO CORPO",
    "CABEÇALHO -> se ab_p EXP_R fc_p então",
    "EXP_R -> OPRD opr OPRD",
    "CORPO -> ES CORPO",
    "CORPO -> CMD CORPO",
    "CORPO -> COND CORPO",
    "CORPO -> fimse",
    "A -> fim"
)

def sintatico():
    if os.path.exists(sys.argv[2]):
        os.remove(sys.argv[2])

    fonte = open(sys.argv[1])
    lexico.fonte(fonte)
    semantico.inicializar()

    pilha = [0]
    a = lexico.proximoToken()

    while (1):
        estado = pilha[-1]

        # Tratamento de erros lexicos
        if a['token'] == 'ERRO':
            erros.emitir(a['descricao'], a)
            pilha, a = erros.panico(pilha, a)
            continue

        comando = acoes[a['token']][estado]
        acao = comando[0]
        acaoNum = int(comando.lstrip('SRAE'))

        if acao == 'S':
            pilha.append(acaoNum)
            semantico.pilha.append(a)
            a = lexico.proximoToken()
        elif acao == 'R':
            producao = producoes[acaoNum-1]
            parte = producao.split(' -> ')

            quantidade = len(parte[1].split(' '))
            del pilha[-quantidade:]

            estado = pilha[-1]
            pilha.append(transicoes[parte[0]][estado])
            print('{:02}. {}'.format(acaoNum, producao))

            token = {'token': parte[0], 'lexema': '', 'tipo': None}
            semantico.analisar(token, acaoNum, quantidade)
        elif acao == 'A':
            break
        else:
            erros.emitir(erros.sintaticos[acaoNum], a)
            pilha, a = erros.panico(pilha, a)
            if a['token'] == 'eof': break

    fonte.close()

    if len(erros.lista):
        print('\nLista de erros:')
        print('\n'.join(erros.lista))
    else:
        with open(sys.argv[2], 'w') as f:
            f.write('\n'.join(semantico.buffer))
        print('\nCompilado com sucesso!')
