#!/usr/bin/env python3
import sys
from os import path

if len(sys.argv) < 2:
    print('Quantidade de parâmetros insuficiente!')
    exit()

fonte = sys.argv[1]
if not path.exists(fonte):
    print('Arquivo fonte "{}" não existe!'.format(fonte))
    exit()

if len(sys.argv) > 2:
    aux = sys.argv[2].split('.')
    if aux[len(aux)-1] != 'c':
        aux.append('c')
    saida = '.'.join(aux)
    sys.argv[2] = saida
else:
    aux = fonte.split('.')
    aux[len(aux)-1] = 'c'
    saida = '.'.join(aux)
    sys.argv.append(saida)

from sintatico import sintatico
print('Compilando "{}" para "{}"...\n'.format(fonte, saida))
sintatico()
