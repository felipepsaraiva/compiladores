import lexico
import sintatico
import semantico

# Lista de erros gerados durante a execução
lista = []

lexicos = (
    'Caractere inválido', # Estado 0
    None,
    'Não há número(s) após o ponto decimal', # Estado 2
    'Não há número(s) após o exponencial', # Estado 3
    'Não há número(s) após o exponencial', # Estado 4
    'Não existe " para finalizar o literal', # Estado 5
    None,
    'Não existe } para finalizar o comentario' # Estado 7
    # Todos os estados seguintes são finais
)

sintaticos = (
    'Inicio do programa não declarado', # 0
    'Fim de arquivo inesperado', # 1
    'Inicio duplicado', # 2
    'Esperado inicio da declaração de variáveis', # 3
    'Bloco de declaração de variáveis duplicado', # 4
    'Declaração de variáveis já foi finalizada', # 5
    '%l encontrado fora de uma estrutura condicional', #6
    '%l encontrado fora do bloco de declarção de variáveis', # 7
    'Constante inesperada: %l', # 8
    'Operador inesperado: %l', # 9
    'Delimitador inesperado: %l', # 10
    'Expressão inválida dentro da declaração de variáveis: %l', # 11
    'Fim inesperado do programa', # 12
    'Identificador da variável não encontrado', # 13
    'Expressão após o fim do programa: %l', # 14
    'Fim do programa duplicado', # 15
    'Esperado encontrar uma variável', # 16
    'Esperado encontrar uma constante ou variável', # 17
    'Esperado encontrar uma atribuição', # 18
    'Esperado encontrar "("', # 19
    'Esperado encontrar ";"', # 20
    'Esperado encontrar um tipo: "int", "real" ou "lit"', # 21
    'Esperado encontrar uma variável, uma constante numérica ou uma expressão matemática', # 22
    'Esperado expressão relacional', # 23
    'Esperado constante numérica ou variável antes do operador relacional', # 24
    'Esperado encontrar ";" ou operador matemático', # 25
    'Esperado encontrar ";", ")" ou um operador  matemático ou relacional', # 26
    'Esperado encontrar ")"', # 27
    'Esperado encontrar operador relacional', # 28
    'Esperado encontrar uma variável ou uma constante numérica', # 29
    'Esperado encontrar "entao"', # 30
)

# A ordem é importante, pois primeiro aparecem nao-terminais que representam menores partes
# do programa e quanto menor for a parte elimidada, mais completa será a análise
sync = ('D', 'LV', 'V', 'ES', 'CMD', 'CABEÇALHO', 'CORPO', 'COND', 'A', 'P')
seguinte = {
    'P': ('eof'),
    'A': ('eof'),
    'V': ('leia', 'escreva', 'id', 'se', 'fim'),
    'LV': ('leia', 'escreva', 'id', 'se', 'fim'),
    'D': ('id', 'varfim'),
    'ES': ('leia', 'escreva', 'id', 'se', 'fim', 'fimse'),
    'CMD': ('leia', 'escreva', 'id', 'se', 'fim', 'fimse'),
    'COND': ('leia', 'escreva', 'id', 'se', 'fim', 'fimse'),
    'CABEÇALHO': ('leia', 'escreva', 'id', 'se', 'fimse'),
    'CORPO': ('leia', 'escreva', 'id', 'se', 'fim', 'fimse')
}


def emitir(mensagem, token):
    lexema = '"{}"'.format(token.get('lexema', ''))
    descricao = mensagem.replace('%l', lexema)
    msg = '[ERRO Ln {}, Col {}] {}.'.format(token['linha'], token['coluna'], descricao)

    print(msg)
    lista.append(msg)


def panico(pilha, entrada):
    """Manipula a pilha e descarta entradas de forma que seja possível continuar a análise sintática"""
    # Encontra um estado que possui uma transição para um dos nao terminais selecionados
    for i in range(len(pilha) - 1, -1, -1):
        s = melhorSync(pilha[i])
        if s: break

    # Desempilha todos os estados até que o estado encontrado seja o topo
    # e empilha o estado resultante da transição
    semantico.pilha = semantico.pilha[:i+1]
    pilha = pilha[:i+1]
    topo = pilha[-1]
    pilha.append(sintatico.transicoes[s][topo])

    # Elimina entradas até encontrar uma que esteja no conjunto seguinte do sync escolhido
    while True:
        entrada = lexico.proximoToken()
        if (entrada['token'] in seguinte[s]) or (entrada['token'] == 'eof'): break

    return pilha, entrada


def melhorSync(estado):
    """Retorna o primeiro não terminal da tupla para o qual o estado tem transição"""
    for s in sync:
        if sintatico.transicoes[s][estado]:
            return s

    return None
