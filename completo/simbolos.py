palavrasReservadas = (
    'inicio', 'varinicio', 'varfim',
    'escreva', 'leia', 'se', 'entao',
    'senao', 'fimse', 'fim',
    'int', 'lit', 'real'
)

tabela = {}
for palavra in palavrasReservadas:
    tabela[palavra] = {'token': palavra, 'lexema': palavra, 'tipo': None}
