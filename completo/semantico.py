import erros
import simbolos

pilha = []
buffer = []

indentacao = 0
inicioTemp = 0
contador = -1 # Inicia no -1 pois ele é incrementado antes de criar a variavel

def inicializar():
    global indentacao, inicioTemp
    escrever('''#include <stdio.h>

typedef char lit[256];

void main(void) {
  /* Variaveis Temporarias */
  /* --------------------- */''')

    indentacao += 1
    inicioTemp = len(buffer) - 1


def escrever(texto):
    for line in texto.split('\n'):
        buffer.append(('  ' * indentacao) + line)


def analisar(token, producao, quantidade):
    global pilha

    args = pilha[-quantidade:]
    pilha = pilha[:-quantidade]
    pilha.append(token)
    regras.get(producao, lambda x, y: 'Default')(token, args)


def criarTemporaria():
    global contador
    contador += 1
    buffer.insert(inicioTemp + contador, '  int T{};'.format(contador))
    return 'T' + repr(contador)


def copiarAtributos(alvo, origem, *args):
    for attr in args:
        alvo[attr] = origem[attr]


# # # # # # # # # # # # # # #
#                           #
# Regras a serem executadas #
# para cada produção        #
#                           #
# # # # # # # # # # # # # # #
def p5(token, args):
    escrever('') # Pula uma linha


def p6(token, args):
    [id, TIPO, _] = args
    simbolos.tabela[id['lexema']]['tipo'] = TIPO['tipo']
    escrever("{} {};".format(TIPO['tipo'], id['lexema']))


def p7(token, args):
    [num] = args
    token['tipo'] = 'int'
    copiarAtributos(token, num, 'linha', 'coluna')


def p8(token, args):
    [num] = args
    token['tipo'] = 'double'
    copiarAtributos(token, num, 'linha', 'coluna')


def p9(token, args):
    [literal] = args
    token['tipo'] = 'lit'
    copiarAtributos(token, literal, 'linha', 'coluna')


def p11(token, args):
    [_, id, _] = args
    tipo = id['tipo']

    if not tipo:
        erros.emitir('Variável %l não declarada', id)

    if tipo == 'lit': escrever('scanf("%s", {});'.format(id['lexema']))
    elif tipo == 'int': escrever('scanf("%d", &{});'.format(id['lexema']))
    elif tipo == 'double': escrever('scanf("%lf", &{});'.format(id['lexema']))


def p12(token, args):
    [_, ARG, _] = args
    escrever('printf({});'.format(ARG['lexema']))


def p13(token, args):
    [literal] = args
    copiarAtributos(token, literal, 'lexema', 'tipo', 'linha', 'coluna')


def p14(token, args):
    [num] = args
    token['lexema'] = '"{}"'.format(num['lexema'])
    copiarAtributos(token, num, 'tipo', 'linha', 'coluna')


def p15(token, args):
    [id] = args
    tipo = id['tipo']

    if not tipo:
        erros.emitir('Variável %l não declarada', id)

    lexema = ''
    if tipo == 'lit': lexema = '"%s", {}'
    elif tipo == 'int': lexema = '"%d", {}'
    elif tipo == 'double': lexema = '"%lf", {}'

    token['lexema'] = lexema.format(id['lexema'])
    copiarAtributos(token, id, 'tipo', 'linha', 'coluna')


def p17(token, args):
    [id, _, LD, _] = args

    if not id['tipo']:
        erros.emitir('Variável %l não declarada', id)

    if not (id['tipo'] == LD['tipo']):
        erros.emitir('Tipos diferentes para atribuição', id)

    escrever('{} = {};'.format(id['lexema'], LD['lexema']))


def p18(token, args):
    [OPRD1, opm, OPRD2] = args

    if OPRD1['tipo'] != OPRD2['tipo'] or OPRD1['tipo'] == 'lit':
        erros.emitir('Operandos com tipos incompatíveis', OPRD1)

    temp = criarTemporaria()
    token['lexema'] = temp
    copiarAtributos(token, OPRD1, 'tipo', 'linha', 'coluna')
    escrever('{} = {} {} {};'.format(temp, OPRD1['lexema'], opm['lexema'], OPRD2['lexema']))


def p19(token, args):
    [OPRD] = args
    copiarAtributos(token, OPRD, 'lexema', 'tipo', 'linha', 'coluna')


def p20(token, args):
    [id] = args

    if not id['tipo']:
        erros.emitir('Variável %l não declarada', id)

    copiarAtributos(token, id, 'lexema', 'tipo', 'linha', 'coluna')


def p21(token, args):
    [num] = args
    copiarAtributos(token, num, 'lexema', 'tipo', 'linha', 'coluna')


def p23(token, args):
    global indentacao
    indentacao -= 1
    escrever('}')


def p24(token, args):
    global indentacao
    [_, _, EXP_R, _, _] = args
    escrever('if ({}) {{'.format(EXP_R['lexema']))
    indentacao += 1


def p25(token, args):
    [OPRD1, opr, OPRD2] = args

    if OPRD1['tipo'] != OPRD2['tipo']:
        erros.emitir('Operandos com tipos incompatíveis', OPRD1)

    temp = criarTemporaria()
    token['lexema'] = temp
    copiarAtributos(token, OPRD1, 'tipo', 'linha', 'coluna')
    escrever('{} = {} {} {};'.format(temp, OPRD1['lexema'], opr['lexema'], OPRD2['lexema']))


def p30(token, args):
    global indentacao
    indentacao -= 1
    escrever('}')


regras = {
    5: p5,
    6: p6,
    7: p7,
    8: p8,
    9: p9,
    11: p11,
    12: p12,
    13: p13,
    14: p14,
    15: p15,
    17: p17,
    18: p18,
    19: p19,
    20: p20,
    21: p21,
    23: p23,
    24: p24,
    25: p25,
    30: p30,
}
