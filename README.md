# Projeto de Compiladores

Projeto desenvolvido para a disciplina de Compiladores. O objetivo é projetar e implementar um compilador de uma linguagem fictícia para a linguagem C, empregando os passos de Análise Léxica, Sintática e Semantica.

## Etapas

O projeto será desenvolvido em três etapas e cada etapa aproveitará o código da etapa anterior:

* Analisador Léxico (`lexico/`)
* Analisador Sintático (`sintatico/`)
* Compilador Completo (`completo/`)

### Analisador Léxico

Para a implementação do analisador léxico, foi contruído um autômato finito que reconhece cada um dos possíveis tokens dependendo do estado que o autômato parar. Esse autômato foi implementado através de uma tabela de transições.

Abaixo estão as listas de tokens que o analisador reconhece:

| Token | Significado | Expressão Regular |
|:---:|:---:|:---:|
| `num` | Constante Numérica | `\d+(\.\d+)?(E(\+\|-)?\d+)?` |
| `literal` | Constante Literal | `".*"` |
| `id` | Identificador | `[a-zA-Z]\w*` |
| `comentario` | Trecho a ser ignorado pelo compilador | `{.*}` |
| `eof` | Fim do Arquivo |  |
| `opr` | Operadores Relacionais | `<\|>\|<=\|>=\|=\|<>` |
| `rcb` | Atribuição | `<-` |
| `opm` | Operadores Aritméticos | `\+\|-\|\*\|\/` |
| `ab_p` | Abre Parênteses | `(` |
| `fc_p` | Fecha Parênteses | `)` |
| `pt_v` | Ponto e Vírgula | `;` |
| `ERRO` | Qualquer coisa diferente de qualquer símbolo token e palavra-chave definida |  |

Lista de palavras reservadas da linguagem e seus significados:

| Palavra | Significado |
|:---:|:---:|
| `inicio` | Delimita o inicio do programa |
| `varinicio` | Delimita o inicio da declaração de variáveis |
| `varfim` | Delimita o fim da declaração de variáveis |
| `escreva` | Imprime na saída padrão |
| `leia` | Lê da entrada padrão |
| `se` | Estrutura condicional |
| `entao` | Elemento de estrutura condicional |
| `senao` | Elemento de estrutura condicional |
| `fimse` | Elemento de estrutura condicional |
| `fim` | Delimita o fim do programa |
| `int` | Tipo de dado (Número inteiro) |
| `lit` | Tipo de dado (Literal/String) |
| `real` | Tipo de dado (Número real) |

### Analisador Sintático

O Analisador Sintático é do tipo ascendente (bottom-up) utilizando o método shift-reduce e o autômato LR(0). A gramática da linguagem fonte é:

| Número | Produção |
|:---:|:---:|
| 1 | `P' -> P` |
| 2 | `P -> inicio V A` |
| 3 | `V -> varinicio LV` |
| 4 | `LV -> D LV` |
| 5 | `LV -> varfim pt_v` |
| 6 | `D -> id TIPO pt_v` |
| 7 | `TIPO -> int` |
| 8 | `TIPO -> real` |
| 9 | `TIPO -> lit` |
| 10 | `A -> ES A` |
| 11 | `ES -> leia id pt_v` |
| 12 | `ES -> escreva ARG pt_v` |
| 13 | `ARG -> literal` |
| 14 | `ARG -> num` |
| 15 | `ARG -> id` |
| 16 | `A -> CMD A` |
| 17 | `CMD -> id rcb LD pt_v` |
| 18 | `LD -> OPRD opm OPRD` |
| 19 | `LD -> OPRD` |
| 20 | `OPRD -> id` |
| 21 | `OPRD -> num` |
| 22 | `A -> COND A` |
| 23 | `COND -> CABEÇALHO CORPO` |
| 24 | `CABEÇALHO -> se ab_p EXP_R fc_p então` |
| 25 | `EXP_R -> OPRD opr OPRD` |
| 26 | `CORPO -> ES CORPO` |
| 27 | `CORPO -> CMD CORPO` |
| 28 | `CORPO -> COND CORPO` |
| 29 | `CORPO -> fimse` |
| 30 | `A -> fim` |

## Analisador Semântico

A análise semântica foi implementada utilizando a tradução dirigida por sintaxe, através de um esquema *s-atribuído*. Para cada produção da gramática, pode existir um conjunto de regras semânticas que devem ser executadas no momento da redução.

Como foi utilizada tradução dirigida por sintaxe, o analisador semântico também é responsável por gerar o programa fonte que será escrito no arquivo final.

## Como utilizar

O script `compilar.py` é um utilitário que permite executar qualquer uma das três etapas do compilador com parâmetros na seguinte ordem:

* **Modo**: O modo em que o compilador será executado (`lexico`, `sintatico` ou `completo`).
* **Arquivo fonte**: Caminho do arquivo fonte que será compilado.
* **Arquivo objeto** *(Opcional)*: Nome do arquivo que será gerado ao fim do processo do **compilador completo** *(Padrão: mesmo nome do arquivo fonte porém com extensão `.c`)*.

Exemplo do comando: `./compilar.py completo fonte.alg saida.c`

Além disso, cada uma das pastas correspondentes às etadas do projeto contem um arquivo `run.py` que pode ser executado diretamente passando como único parâmetro o arquivo que será compilado. Adicionalmente, o compilador completo aceita um parâmetro opcional para o nome do arquivo gerado.

## Exemplo

#### Programa fonte:

```
inicio
    varinicio
        A lit;
        B int;
        D int;
        C real;
    varfim;

    escreva "Digite A: ";
    leia A;
    escreva "Digite B: ";
    leia B;

    se (B > 2) entao
        se (B < 7) entao
            escreva "B esta entre 2 e 7";
        fimse
    fimse

    D <- B;
    B <- B+1;
    B <- B+2;
    B <- B+3;
    C <- 5.0;
    C <- C+2.5;

    escreva "\nA = ";
    escreva A;
    escreva "\nB = ";
    escreva B;
    escreva "\nC = ";
    escreva C;
    escreva "\nD = ";
    escreva D;
    escreva "\n";
fim
```

#### Produto final do compilador completo:

```C
#include <stdio.h>

typedef char lit[256];

void main(void) {
  /* Variaveis Temporarias */
  int T0;
  int T1;
  int T2;
  int T3;
  int T4;
  int T5;
  /* --------------------- */
  lit A;
  int B;
  int D;
  double C;

  printf("Digite A: ");
  scanf("%s", A);
  printf("Digite B: ");
  scanf("%d", &B);

  T0 = B > 2;
  if (T0) {
    T1 = B < 7;
    if (T1) {
      printf("B esta entre 2 e 7");
    }
  }

  D = B;
  T2 = B + 1;
  B = T2;
  T3 = B + 2;
  B = T3;
  T4 = B + 3;
  B = T4;
  C = 5.0;
  T5 = C + 2.5;
  C = T5;

  printf("\nA = ");
  printf("%s", A);
  printf("\nB = ");
  printf("%d", B);
  printf("\nC = ");
  printf("%lf", C);
  printf("\nD = ");
  printf("%d", D);
  printf("\n");
}
```
