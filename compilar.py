#!/usr/bin/env python3
import sys
from os import path, system

if len(sys.argv) < 3:
    print('Quantidade de parâmetros insuficiente!')
    exit()

modo = sys.argv[1]
if modo not in ['lexico', 'sintatico', 'completo']:
    print('Modo "{}" não existe!'.format(modo))
    exit()

fonte = sys.argv[2]
if not path.exists(fonte):
    print('Arquivo fonte "{}" não existe!'.format(fonte))
    exit()

command = './{}/run.py {}'.format(modo, fonte)
if len(sys.argv) > 3:
    command = '{} {}'.format(command, sys.argv[3])

system(command)
